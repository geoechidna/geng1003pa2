/* Gage Wilkins -- c3184275
 * GENG1003 Assignment 02 -- 01/10/14
 *
 * Compile cricket team bowling results based on text file inputs.
 */

// Replaces stdio.h, for input/output
#include "stdafx.h"
// For strcmp(), strcpy(), strlen()
#include <string.h>

// Size of a team
#define TEAMSIZE 15
// Maximal length for a name
#define NAMESIZE 21
// Path to names file
#define PATH_A "U:\\GENG1003\\input2a.dat"
// Path to scores file
#define PATH_B "U:\\GENG1003\\input2b.dat"

// Holds all data about a player
typedef struct player{
	// Player's surname
	char name[NAMESIZE];
	// Number of overs played
	int overs,
	// Number of maiden overs
		maidens,
	// Number of runs conceded
		runs,
	// Number of wickets taken
		wickets;
	// Deliveries per wicket taken
	double strikeRate,
	// Runs conceded per wicket taken
		average;
// Define player as struct player
}player;

// Function prototypes
int checkTeamList(player team[], player p);
void print(player p);
player* copy(player old[]);
void sortAlpha(player team[]);
void sortPerformance(player team[]);
int scrcmp(player p1, player p2);

// Entry point for application
int main(void){

	// Holds error statuses for file loading
	errno_t err;
	// Will be pointers to both datafiles
	FILE *file, *file2;
	// Attempt to open names file and store the error status
	err = fopen_s(&file, PATH_A, "r");

	// If there was an error opening the file...
	if (err != 0){
		// ... print error message...
		printf("No file at " PATH_A ". Exiting . . .\n");
		// ... and exit
		return -1;
	}

	// Holds the latest games' data for each player
	player *teamLatest = new player[TEAMSIZE];
	// For each player in the array, initialise the data to 0 or ""
	for (int i = 0; i < TEAMSIZE; i++){
		strcpy_s(teamLatest[i].name, 1, "");
		teamLatest[i].overs = 0;
		teamLatest[i].maidens = 0;
		teamLatest[i].runs = 0;
		teamLatest[i].wickets = 0;
		teamLatest[i].strikeRate = 0;
		teamLatest[i].average = 0;
	}
	
	// For each player in the team...
	for (int i = 0; i < TEAMSIZE; i++){
		// String to hold the name from file
		char *name = new char[NAMESIZE*2];
		// Read up to maximal name size from file...
		fscanf_s(file, "%s", name, NAMESIZE*2);
		// const char[] needed to use strlen()
		const char *cname = name;
		// If the name is 1 or less characters i.e. "\0" or ""...
		if (strlen(cname) <= 1){
			// ... print error message...
			printf("Empty Line in name list, ignoring\n");
			// ... and decrease the count, so the same player slot is used again
			i--;
		}
		// Otherwise, if the name is too long...
		else if (strlen(cname) >= NAMESIZE){
			// ... print error message...
			printf("%s - name is too long\n", cname);
			// ... and decrease the count, so the same player slot is used again
			i--;
		}
		// Otherwise, if there's no error...
		else{
			// ... copy the name in to the array
			strcpy_s(teamLatest[i].name, NAMESIZE, cname);
		}
	}

	// Holds the overall scores for each player
	// Initialised through copying the already initialised array, with names already inputted
	player *teamOverall = copy(teamLatest);

	// Attempt to open scores file and store the error status
	err = fopen_s(&file2, PATH_B, "r");

	// If there was an error opening the file...
	if (err != 0){
		// ... print error message...
		printf("No file at " PATH_B ". Exiting . . .\n");
		// ... and exit
		return -1;
	}

	// Create new player to receive data from file, initialised to 0
	player p = { "", 0, 0, 0, 0, 0, 0 };

	// Read data from file, and store it into p. Stop when it reaches the end of file
	while (fscanf_s(file2, "%s %d %d %d %d", p.name, NAMESIZE, &p.overs, &p.maidens, &p.runs, &p.wickets) != EOF){
		// Get index of player from function
		int playerPos = checkTeamList(teamLatest, p);
		// If the player was not found in the list...
		if (playerPos == -1){
			// ... print the data...
			print(p);
			// ... with an error message
			printf(" - name not found in team list\n");
		}
		// Check that all data is valid: no negative values, no more maidens than overs, no more than 10 wickets
		else if (p.maidens < 0 ||
			p.overs < p.maidens ||
			p.runs < 0 ||
			p.wickets < 0 ||
			p.wickets > 10){
			// If it is invalid, print the data...
			print(p);
			// ... with an error message
			printf(" - data is invalid\n");
		}
		// Otherwise, if the data is valid, and the player is on the list...
		else{
			// Store all data into the latest score for that player
			teamLatest[playerPos] = p;
			// Add each data point to the total scores for the player
			teamOverall[playerPos].maidens += p.maidens;
			teamOverall[playerPos].overs += p.overs;
			teamOverall[playerPos].runs += p.runs;
			teamOverall[playerPos].wickets += p.wickets;
			// If they have taken at least 1 wicket...
			if (teamOverall[playerPos].wickets > 0){
				// ... calculate the average runs/wicket...
				teamOverall[playerPos].average = (double)teamOverall[playerPos].runs / teamOverall[playerPos].wickets;
				// ... and the average deliveries/wicket
				teamOverall[playerPos].strikeRate = ((double)teamOverall[playerPos].overs * 6) / teamOverall[playerPos].wickets;
			}
			// If they havent taken any wickets...
			else{
				// ... strike rate and average are 0
				teamOverall[playerPos].average = 0;
				teamOverall[playerPos].strikeRate = 0;
			}
		}
	}

	// Sort team alphabetically
	sortAlpha(teamOverall);

	// Print results:
	// First, alphabetical
	// Print top border
	printf(".-------------------------------------------------------------------------.\n");
	// Print headings
	printf("| ALPHABETICAL                   Overall                     Latest       |\n");
	printf("|        NAME	          O    M    R    W    Avg   S/R    O   M    R   W |\n");
	// For each player...
	for (int i = 0; i < TEAMSIZE; i++){
		// Store their data into temporary output players for readability
		player O = teamOverall[i];
		player L = teamLatest[checkTeamList(teamLatest, teamOverall[i])];
		// Print overall data in given format
		printf("| %20s  %3d  %3d  %3d  %3d  ", O.name, O.overs, O.maidens, O.runs, O.wickets);
		// Only print average and strike rate if they exist
		if (O.wickets > 0) printf("%6.2f  %3d  ", O.average, (int)O.strikeRate);
		// Otherwise pad with spaces
		else printf("             ");
		// Print latest game data in given format
		printf("%3d  %2d  %3d  %2d |\n", L.overs, L.maidens, L.runs, L.wickets);
	}
	// Finish border
	printf("`-------------------------------------------------------------------------'\n");

	// Change sorting to by performance
	sortPerformance(teamOverall);

	// Print results:
	// Second, by performance, overall
	// Print top border
	printf(".-------------------------------------------------------------------------.\n");
	// Print headings
	printf("| BY PERFORMANCE                 Overall                     Latest       |\n");
	printf("|        NAME	          O    M    R    W    Avg   S/R    O   M    R   W |\n");
	// For each player...
	for (int i = 0; i < TEAMSIZE; i++){
		// Store their data into temporary output players for readability
		player O = teamOverall[i];
		player L = teamLatest[checkTeamList(teamLatest, teamOverall[i])];
		// Print overall data in given format
		printf("| %20s  %3d  %3d  %3d  %3d  ", O.name, O.overs, O.maidens, O.runs, O.wickets);
		// Only print average and strike rate if they exist
		if (O.wickets > 0) printf("%6.2f  %3d  ", O.average, (int)O.strikeRate);
		// Otherwise pad with spaces
		else printf("             ");
		// Print latest game data in given format
		printf("%3d  %2d  %3d  %2d |\n", L.overs, L.maidens, L.runs, L.wickets);
	}
	// Print bottom of border
	printf("`-------------------------------------------------------------------------'\n");
	
	// Print results:
	// Last, by performance, overall, where they have played at least one over
	// Print top border
	printf(".-------------------------------------------------------------------------.\n");
	// Print headings
	printf("| BY PERFORMANCE                                                          |\n");
	printf("| AT LEAST ONE OVER PLAYED       Overall                     Latest       |\n");
	printf("|        NAME	          O    M    R    W    Avg   S/R    O   M    R   W |\n");
	// For each player...
	for (int i = 0; i < TEAMSIZE; i++){
		// Store their data into temporary output players for readability
		player O = teamOverall[i];
		player L = teamLatest[checkTeamList(teamLatest, teamOverall[i])];
		if (O.overs > 0){
			// Print overall data in given format
			printf("| %20s  %3d  %3d  %3d  %3d  ", O.name, O.overs, O.maidens, O.runs, O.wickets);
			// Only print average and strike rate if they exist
			if (O.wickets > 0) printf("%6.2f  %3d  ", O.average, (int)O.strikeRate);
			// Otherwise pad with spaces
			else printf("             ");
			// Print latest game data in given format
			printf("%3d  %2d  %3d  %2d |\n", L.overs, L.maidens, L.runs, L.wickets);
		}
	}
	// Print bottom of border	
	printf("`-------------------------------------------------------------------------'\n");

	//getchar();
	// Close all files
	_fcloseall();
}


/*************\
 * FUNCTIONS *
\*************/

/* checkTeamList()
 * checks to see if the name of player p matches with any player in team[], and returns the index
 * Parameters:
	player team[]:	array of players to search for a name in
	player p:		player with name to search for
 * Returns:
	int				index of the player whose name matches, or -1 if no match
 */
int checkTeamList(player team[], player p){
	// For each player
	for (int i = 0; i < TEAMSIZE; i++)
		// Check if the names match
		if (!strcmp(p.name, team[i].name))
			// and if they do, return the index
			return i;
	// Otherwise return -1
	return -1;
}

/* print() 
 * prints a player struct
 * Parameters
	player p:	the player struct to be printed
 */
void print(player p){
	// Print with formatting
	printf("%-20s- O: %3d, M: %3d, R:%3d, W:%3d", p.name, p.overs, p.maidens, p.runs, p.wickets);
}

/* copy()
 * creates a copy of a given player array, and returns it
 * Parameters
	player old[]:	the array to be copied
 * Returns
	player*:		a pointer to the new array
 */
player* copy(player old[]){
	// create new array
	player *ret = new player[TEAMSIZE];
	// Populate it with data from the original
	for (int i = 0; i < TEAMSIZE; i++){
		strcpy_s(ret[i].name, NAMESIZE, old[i].name);
		ret[i].wickets = old[i].wickets;
		ret[i].maidens = old[i].maidens;
		ret[i].runs = old[i].runs;
		ret[i].overs = old[i].overs;
		ret[i].average = old[i].average;
		ret[i].strikeRate = old[i].strikeRate;
	}
	// return the pointer to the new array
	return ret;
}

/* sortAlpha()
 * sorts a given player array alphabetically by name
 * Parameters
	player team[]:	the array to be sorted
 * Returns
	player team[]:	the array sorted
 */
void sortAlpha(player team[]){
	// For each player...
	for (int i = 0; i < (TEAMSIZE - 1); i++){
		// Compared to each other player...
		for (int j = 0; j < (TEAMSIZE - i - 1); j++){
			// If the name of the first player is after the second player
			if (strcmp(team[j].name, team[j + 1].name) == 1){
				// swap them
				player swap = team[j];
				team[j] = team[j + 1];
				team[j + 1] = swap;
			}
		}
	}
}

/* sortPerformance()
 * sorts a given player array by score, calculated in scrcmp()
 * Parameters
	player team[]:	the array to be sorted
 * Returns
	player team[]:	the array sorted
 */
void sortPerformance(player team[]){
	// For each player...
	for (int i = 0; i < (TEAMSIZE - 1); i++){
		// Compared to each other player...
		for (int j = 0; j < (TEAMSIZE - i - 1); j++){
			// If the score of the first player is less than the second player
			if (scrcmp(team[j], team[j + 1]) == 1){
				// swap them
				player swap = team[j];
				team[j] = team[j + 1];
				team[j + 1] = swap;
			}
		}
	}
}

/* scrcmp()
 * score compare: compares two players' scores
 * Paramters
	player p1, p2:	the players to compare
 * Returns
	int				1 if first player is smaller
					0 if equal
				   -1 if second player is smaller
 */
int scrcmp(player p1, player p2){

	// If 1 player has played no overs, they must have a lower score
	// If both have played no overs, they will be compared by strcmp()
	if (p1.overs == p2.overs);
	else if (p1.overs == 0)
		return 1;
	else if (p2.overs == 0)
		return -1;

	// If both have played at least one over, the one who took less wickets will have a lower score
	if (p1.wickets < p2.wickets)
		return 1;
	else if (p2.wickets < p1.wickets)
		return -1;
	else
		// If both have played at least one over and have taken the same amount of wickets, the one
		// who took less runs will have a lower score
		if (p1.runs > p2.runs)
			return 1;
		else if (p1.runs < p2.runs)
			return -1;
		// If both have played at least one over and have taken the same amount of wickets and
		// have concede the same amount of runs, or neither have played an over, they will be sorted
		// alphabetically
		else return strcmp(p1.name, p2.name);
}